//import com.sun.java_cup.internal.runtime.Scanner;
import java.util.Scanner;

public class Operators {
    public static void main(String[] args) {
        int x = 5;
        x++;
        System.out.println(x);

        ++x;
        System.out.println(x);

        //++x and x++ increments the same

        //User input
        Scanner in = new Scanner(System.in);
        String wineChoice = in.nextLine();

        //Use of switch
        switch(wineChoice){
            case "Dornfelder":
                System.out.println("Terrific");
                break;
            case "Pino Grigio":
                System.out.println("Awesome");
                break;
            case "Pinotage":
                System.out.println("Oh wow!");
                break;
            case "Zinfandel":
                System.out.println("Great");
                break;
            default :
                System.out.println("Never heard");
        }

        //Swap numbers
        int a = 5;
        int b = 7;
        int c;

        c = a;
        a = b;
        b = c;
        System.out.println(a);
        System.out.println(b);

    }

}
