import java.util.Scanner; //import a scanner class to read the keyboard input

public class Input_Homework19Apr {
    String petType, dogName, dogBreed;
    int dogAge;

    //method to talk
    public static String petTalk(String dogBreed) {
        switch(dogBreed.toUpperCase()){
            case "CHIHUAHUA" :
                return "Huahua!";
            case "POODLE" :
                return "Poopoo!";
            case "GOLDEN RETRIEVER" :
                return "Goldie!";
            case "ALASKAN MALAMUTE" :
                return "Alas!";
            default :
                return "Meowmeow!";
        }
    }

    public static void main(String[] args) {
        Input_Homework19Apr myDog = new Input_Homework19Apr(); //

        Scanner input = new Scanner(System.in);

        System.out.println("What's your dog's name?"); //****prompt message
        myDog.dogName = input.nextLine(); //read a string and store it to dogName

        //capitalize the first letter of the name
        String nameUpper = myDog.dogName.substring(0, 1).toUpperCase() + myDog.dogName.substring(1).toLowerCase();

        System.out.println("What's " + nameUpper + "'s age?"); //****prompt message
        myDog.dogAge = Integer.parseInt(input.nextLine()); //read an int and store it to dogAge

        System.out.println("What's your dog's breed?"); //****prompt message
        myDog.dogBreed = input.nextLine(); //read a string and store it to dogBreed

        System.out.println("Hi " + nameUpper + "!"); //prints out the name
        System.out.println(nameUpper + " is " + myDog.dogAge + " year/s old."); //prints out the name and age

        System.out.printf(nameUpper + " is a " + myDog.dogBreed.substring(0, 1).toUpperCase() + myDog.dogBreed.substring(1).toLowerCase() +  " and says " + petTalk(myDog.dogBreed));
    }
}