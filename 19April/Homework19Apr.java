public class Homework19Apr {
    int age;
    String name, breed, petType;

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    //method to show how they talk - if else practice
    private static String petTalk(String petType) {
        switch(petType){
            case "Chihuahua" :
                return "Huahua";
            case "Poodle" :
                return "Poopoo";
            case "Golden retriever" :
                return "Goldie";
            case "Alaskan Malamute" :
                return "Alas";
            default :
                return "Meowmeow";
        }
    }

    public static void main(String[] args) {
        Homework19Apr myDog = new Homework19Apr();

        //sets the properties
        myDog.setName("Beethoven");
        myDog.setAge(3);
        myDog.setBreed("Cat");

        //prints out the name and age
        System.out.println("Hi " + myDog.name + "!");
        System.out.println(myDog.name + " is " + myDog.age + " years old!");

        //prints out the name, breed and how they talk - switch practice
        switch(myDog.breed){
            case "Chihuahua" :
                System.out.printf(myDog.name + " is a Chihuahua\n");
                System.out.println(myDog.name + " says " + petTalk("Chihuahua"));
                break;
            case "Poodle" :
                System.out.printf(myDog.name + " is a Poodle\n");
                System.out.println(myDog.name + " says " + petTalk("Poodle"));
                break;
            case "Golden Retriever" :
                System.out.printf(myDog.name + " is a Golden Retriever\n");
                System.out.println(myDog.name + " says " + petTalk("Golden Retriever"));
                break;
            case "Alaskan Malamute" :
                System.out.printf(myDog.name + " is an Alaskan Malamute\n");
                System.out.println(myDog.name + " says " + petTalk("Alaskan Malamute"));
                break;
            default :
                System.out.printf(myDog.breed + " is also cute, " + myDog.name + "!\n");
                System.out.println(myDog.name + " says " + petTalk("cat"));
        }
    }

}
