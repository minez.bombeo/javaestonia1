package HW_25Apr;

public class User {
    private String userName, userGender;
    private double userID;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public void setUserID(double userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getUserGender() {
        return this.userGender;
    }

    public double getUserID() {
        return this.userID;
    }
}
