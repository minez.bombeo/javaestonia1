package HW_25Apr;

public class Dog {
    private String petName, petBreed;
    private int petAge, dogCount;

    public String setPetName(String petName) {
        this.petName = petName;
        return petName;
    }

    public String setPetBreed(String petBreed) {
        this.petBreed = petBreed;
        return petBreed;
    }

    public int setPetAge(int petAge) {
        this.petAge = petAge;
        return petAge;
    }

    public void setDogCount(int dogCount) {
        this.dogCount = dogCount;
    }

    public String getPetName() {
        return petName;
    }

    public String getPetBreed() {
        return petBreed;
    }

    public int getDogCount() {
        return dogCount;
    }

    public int getPetAge() {
        return petAge;
    }
}
