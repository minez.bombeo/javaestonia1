public class LoopExercise {
    public static int GetBigger(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public static boolean IsLeapYear(int year) {
        /*if (year % 400 == 0 || year % 4 == 0 && year % 100 !=0){
            return true;
        } else {
            return false;
        }*/

        return year % 400 == 0 || year % 4 == 0 && year % 100 !=0;
    }

    public static boolean IsNotLeapYear(int year) {
        return !IsLeapYear(year);
    }


    public static void main(String[] args) {
        /*int bigger = GetBigger( 2, 3);
        System.out.println(bigger);
        bigger = GetBigger(3, 2);
        System.out.printf(bigger);*/

        boolean Leap = IsLeapYear(2021);
        System.out.println(Leap);

        }
}
