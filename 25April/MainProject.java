package HW_25Apr;

import java.util.Scanner;

public class MainProject {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        User user = new User();
        Greet greet = new Greet();

        //User info
        System.out.print("Name : ");
        user.setUserName(input.nextLine()); //store in userName

        System.out.print("ID : ");
        user.setUserID(Double.parseDouble(input.next())); //store in userID

        System.out.print("Gender : ");
        user.setUserGender(input.next());

        //Pet info
        System.out.print("How many dogs do you have? ");
        int dogCount = input.nextInt(); //read number of dogs
        Dog[] dogs = new Dog[dogCount];

        if (dogCount > 0) {
            for (int i = 0; i < dogCount; i++){
                String name = "animal" + i;
                System.out.print ("Input age : ");
                int age = input.nextInt();

                //Dog dogA = new Dog(name, age);
                dogs[i] = new Dog(name, age); //same as Animal animal = new Animal(name, age) array[i] = new Dog(name, age)
            }
            //int totalAge = 0;
            //for(Dog dogX : dogs) {
            //    totalAge = totalAge + dogX.getPetAge(); //totalAge += dogX.getPetAge()
            //}
            //int average = totalAge/dogCount;
            //System.out.println(average);

            System.out.print("What breed is the eldest dog? ");
            greet.petTalk(input.next().toUpperCase());
        } else {
            System.out.print("How many cats do you have then?");
            String ownedCat = input.next();
        }
    }
}