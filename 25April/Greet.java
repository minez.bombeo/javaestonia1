package HW_25Apr;

public class Greet {
    public static String petTalk(String dogBreed) {
        switch(dogBreed){
            case "CHIHUAHUA" :
                System.out.print("The oldest dog says Chi!");
                break;
            case "POODLE" :
                System.out.print("The oldest dog says Poo!");
                break;
            case "GOLDEN RETRIEVER" :
                System.out.print("The oldest dog says Goldie!");
                break;
            case "ALASKAN MALAMUTE" :
                System.out.print("The oldest dog says Alas!");
                break;
            default :
                System.out.println("The dog says Meowmeow!");
        }
        return dogBreed;
    }
}
