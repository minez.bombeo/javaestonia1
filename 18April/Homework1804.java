public class Homework1804 {
    public String dogName;
    public int dogAge;

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public void setDogAge(int dogAge) {
        this.dogAge = dogAge;
    }

    public static void main(String[] args) {
        Homework1804 dog = new Homework1804();

        dog.setDogName("Banowati");
        System.out.println(dog.dogName);

        dog.setDogAge(4);
        System.out.println(dog.dogAge);
    }
}
