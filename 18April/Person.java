public class Person {
    public int idCode;
    public String name;

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.setName("Minez");
        person.setName("Mimi");
        person.name = "Miminez";

        System.out.println(person.name);
    }
}
